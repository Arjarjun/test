import React, { Component } from 'react'
import {connect} from 'react-redux'

const component = (props) => {
    console.log("state is", props.state)

   const showAvailableConnection = (data,del) => {
      console.log("show data",data)
      let returnData;
      if (data.length){
        returnData =(data.map(curr=>(
            <div>
                <p>{curr.email}</p>
                <button onClick={
                    () => del(curr.id)
                }>delete</button>
            </div>
        )))
      }
      else{
          returnData = (<div>
              <p>Sorry No Connection Available</p>
          </div>)
      }
        return returnData
    }

    return(<div>
        <h1>Facebook</h1>
        <div>
            <p>Add Connection</p>
            <input value={props.state.email} onChange={
                (name) => props.updatetypedEmail(name)
            }/>
            <button onClick={
                ()=>props.addConnection(props.state.email,props.state.id)
            } >add</button>
            
            {showAvailableConnection(props.state.connections, props.del)}

        </div>
    </div>)
}



const mapStateToProps = (state) =>{
    return{
        state:state.facebookReducer
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        addConnection: (name,id) =>{
            dispatch({
                type:'add',
                payload:{
                   data:{
                    email:name,
                    id:id+1
                   },
                   id:id+1
                }
            })
        },
        updatetypedEmail: name => {
            dispatch({
                type:'name',
                payload: name.target.value
            })
        },
        del: id => {
            dispatch({
                type:'delete',
                payload:{
                    id: id
                }
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component)
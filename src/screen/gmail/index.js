import React, { Component } from 'react'
import {connect} from 'react-redux'

const component = (props) => {
    console.log("state is", props.state)

   const showAvailableConnection = (data,del) => {
      console.log("show data",data)
      let returnData;
      if (data.length){
        returnData =(data.map(curr=>(
            <div>
                <p>{curr.email}</p>
                <button onClick={
                    () => del(curr.id)
                }>delete</button>
            </div>
        )))
      }
      else{
          returnData = (<div>
              <p>Sorry No Connection</p>
          </div>)
      }
        return returnData
    }

    return(<div>
        <h1>Gmail</h1>
        <div>
            <p>Add Connection</p>
            <input value={props.state.typedEmail} onChange={
                (name) => props.updatetypedEmail(name)
            }/>
            <button onClick={
                ()=>props.addConnection(props.state.typedEmail,props.state.id)
            } >add</button>
            
            {showAvailableConnection(props.state.connections, props.del)}

        </div>
    </div>)
}



const mapStateToProps = (state) =>{
    return{
        state:state.gmailReducer
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        addConnection: (name,id) =>{
            dispatch({
                type:'addGmail',
                payload:{
                   data:{
                    email:name,
                    id:id+1
                   },
                   id:id+1
                }
            })
        },
        updatetypedEmail: name => {
            dispatch({
                type:'typedEmail',
                payload: name.target.value
            })
        },
        del: id => {
            dispatch({
                type:'deleteGmail',
                payload:{
                    id: id
                }
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component)
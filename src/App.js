import React, { Component } from 'react';
import logo from './logo.svg';
import Connections from './screen/connection'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Connections/>
      </div>
    );
  }
}

export default App;

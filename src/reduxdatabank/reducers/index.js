import {
    combineReducers
} from 'redux'

const gmailState ={
        connections:[],
        id:0,
        previous:null,
        typedEmail:''
}

const facebookState = {
    connections:[],
    id:0,
    previous:null,
    email:''
}

const gmailReducer = (state=gmailState, action) => {
    if(action.type==='listGmail'){
        return Object.assign({},state,{
            connections:action.payload
        })
    }
    if(action.type==='deleteGmail'){
        const conn = state.connections
        return Object.assign({},state,{
            connections:conn.filter(item=>item.id!=action.payload.id),
            
        })
    }
    if(action.type==='addGmail'){
        return Object.assign({}, state,{
            connections:[...state.connections, action.payload.data],
            previous:action.payload.data.email,
            id:action.payload.id,
            typedEmail:''
        })
    }
    if(action.type==='typedEmail'){
        return Object.assign({}, state,{
            typedEmail:action.payload
        })
    }
    return state
}

const facebookReducer = (state=facebookState, action) =>{
    if(action.type==='list'){
        return Object.assign({},state,{
            connections:action.payload
        })
    }
    if(action.type==='delete'){
        console.log(conn)
        const conn = state.connections
        return Object.assign({},state,{
            connections:conn.filter(item=>item.id!=action.payload.id),
            
        })
    }
    if(action.type==='add'){
        console.log("called")
        return Object.assign({}, state,{
            connections:[...state.connections, action.payload.data],
            previous:action.payload.data.email,
            id:action.payload.id,
            email:''
        })
    }
    if(action.type==='name'){
        return Object.assign({}, state,{
            email:action.payload,
            
        })
    }
    return state
}

export default combineReducers({
    gmailReducer,
    facebookReducer
})

